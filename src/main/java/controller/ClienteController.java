package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import model.Cliente;
 
@ControllerAdvice
public class ClienteController {
	
	@Autowired
	private Cliente cl;
	
	
@RequestMapping("cliente")
public ModelAndView cliente(){
	ModelAndView modelAndView = new ModelAndView();
	modelAndView.setViewName("cliente");
	return modelAndView;
}
	
@RequestMapping("saveCliente")
public ModelAndView saveCliente(String nom, String ap1, String ap2, String mail,
		String edo, String indus, char pol){
	ModelAndView modelAndView = new ModelAndView();
	cl = new Cliente();
	cl.setNOMBRE(nom);
	cl.setAP1(ap1);
	cl.setAP2(ap2);
	cl.setEMAIL(mail);
	cl.setEDO(edo);
	cl.setINDUSTRIA(indus);
	cl.setPOLITICAS(pol);
	return modelAndView;
	
}
	
}
