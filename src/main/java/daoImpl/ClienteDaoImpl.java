package daoImpl;

import javax.management.Query;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import dao.ClienteDao;
import model.Cliente;

@Repository("ClienteDao")
public class ClienteDaoImpl implements ClienteDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public ClienteDaoImpl(SessionFactory sessionFactory){
		this.sessionFactory =sessionFactory;
	}
	
	public void saveCliente(Cliente c) {
		sessionFactory.getCurrentSession().save(c);
		
	}

	
	public Cliente getClienteByNombre(final String nombre) {
		Cliente cl = (Cliente) sessionFactory.getCurrentSession().createQuery("FROM USER * WHERE NOMBRE=?;")
				.setParameter(0, nombre);
		
		return cl;
	}

}
