package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
@Entity
@Table(name="Cliente")
public class Cliente  {
	@Id
	@GeneratedValue(generator= "gen")
	@GenericGenerator(name = "gen", strategy = "increment", parameters = @Parameter(value = "cliente", name = "property"))
	private Long PK_CLIENTE;
	@Column(length = 100)
	private String NOMBRE;
	@Column(length =100)
	private String AP1;
	@Column(length = 100)
	private String AP2;
	@Column (length = 100)
	private String EMAIL;
	@Column (length = 100)
	private String EDO;
	@Column (length = 100)
	private String INDUSTRIA;
	public Long getPK_CLIENTE() {
		return PK_CLIENTE;
	}
	public void setPK_CLIENTE(Long pK_CLIENTE) {
		PK_CLIENTE = pK_CLIENTE;
	}
	public String getNOMBRE() {
		return NOMBRE;
	}
	public void setNOMBRE(String nOMBRE) {
		NOMBRE = nOMBRE;
	}
	public String getAP1() {
		return AP1;
	}
	public void setAP1(String aP1) {
		AP1 = aP1;
	}
	public String getAP2() {
		return AP2;
	}
	public void setAP2(String aP2) {
		AP2 = aP2;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getEDO() {
		return EDO;
	}
	public void setEDO(String eDO) {
		EDO = eDO;
	}
	public String getINDUSTRIA() {
		return INDUSTRIA;
	}
	public void setINDUSTRIA(String iNDUSTRIA) {
		INDUSTRIA = iNDUSTRIA;
	}
	public char getPOLITICAS() {
		return POLITICAS;
	}
	public void setPOLITICAS(char pOLITICAS) {
		POLITICAS = pOLITICAS;
	}
	private char POLITICAS;




}
