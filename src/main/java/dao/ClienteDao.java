
package dao;

import model.Cliente;


public interface ClienteDao {

	 void saveCliente (Cliente c);
	 
	  Cliente getClienteByNombre(String nombre);
}
